import org.junit.Assert;
import org.junit.Test;

import io.restassured.RestAssured;
import io.restassured.response.Response;


public class Example {
	
	String baseURI = "http://dummy.restapiexample.com/";
	Response getResponse;
	
	@Test
	public void getRequest(){
		getResponse = RestAssured
			.given()
				.baseUri(baseURI)
			.when()
				.get("api/v1/employees");
		
		String responseBody = getResponse.asString();
		// Printing response as string
		System.out.println(responseBody);
		
		// Printing the pretty print
		getResponse.prettyPrint();
		
		int actualStatusCode = getResponse.getStatusCode();
		int expectedStatusCode = 200;
		System.out.println("Actual Status Code is: " + actualStatusCode);
		Assert.assertEquals(expectedStatusCode, actualStatusCode);
		
		if(responseBody.contains("success")) {
			System.out.println("Body contains success");
			Assert.assertTrue("Test Case Passed", true);
		} else {
			System.out.println("Body does not contain success");
			Assert.assertTrue("Test Case Failed", false);
		}
	}
	

}
